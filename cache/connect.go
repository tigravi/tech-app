package cache

import (
	"github.com/redis/go-redis/v9"
	"github.com/spf13/viper"
)

var (
	client   *redis.Client
	ring     *redis.Ring
	cluster  *redis.ClusterClient
	sentinel *redis.SentinelClient
)

func InitCache() {

	switch viper.GetString("cache.driver") {
	case "redis-client":
		client = redis.NewClient(&redis.Options{
			Addr:     viper.GetString("cache.host"),
			Password: viper.GetString("cache.password"), // no password set
			DB:       viper.GetInt("cache.db"),          // use default DB
		})
	case "redis-ring":
		ring = redis.NewRing(&redis.RingOptions{
			Addrs: map[string]string{
				// shardName => host:port
				"shard1": "localhost:7000",
				"shard2": "localhost:7001",
				"shard3": "localhost:7002",
			},
			Username: "",
			Password: "",
			DB:       0,
		})
	case "redis-cluster":
		cluster = redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{":7000", ":7001", ":7002", ":7003", ":7004", ":7005"},

			// To route commands by latency or randomly, enable one of the following.
			//RouteByLatency: true,
			//RouteRandomly: true,
		})
		//err := rdb.ForEachShard(ctx, func(ctx context.Context, shard *redis.Client) error {
		//	return shard.Ping(ctx).Err()
		//})
		//if err != nil {
		//	panic(err)
		//}
		//rdb := redis.NewClusterClient(&redis.ClusterOptions{
		//	NewClient: func(opt *redis.Options) *redis.NewClient {
		//		user, pass := userPassForAddr(opt.Addr)
		//		opt.Username = user
		//		opt.Password = pass
		//
		//		return redis.NewClient(opt)
		//	},
		//})
	case "redis-sentinel":
		sentinel = redis.NewSentinelClient(&redis.Options{
			Addr: ":9126",
		})
		//sentinel := redis.NewFailoverClusterClient(&redis.FailoverOptions{
		//	MasterName:    "master-name",
		//	SentinelAddrs: []string{":9126", ":9127", ":9128"}})

		//addr, err := sentinel.GetMasterAddrByName(ctx, "master-name").Result()

	}

}

func Handler() *redis.Client {
	return client
}

func Get(key string) (string, error) {

	return "", nil
}

func Set(key string, value string, ttl int) error {
	return nil
}
