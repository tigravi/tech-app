package tech_app

import (
	"encoding/json"
	"errors"
	"github.com/spf13/viper"
	"gitlab.com/tigravi/tech-app/logger"
	"gitlab.com/tigravi/tech-app/router"
	"golang.org/x/exp/slog"
	"golang.org/x/net/context"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

type HttpServer struct {
	Server   *http.Server
	Log      *slog.Logger
	Handlers http.Handler
}
type ResponsError struct {
	Error string `json:"error"`
}
type ResponsData struct {
	Data string `json:"error"`
}

var (
	App *HttpServer
)

func init() {
	App = NewHttpSever()
}
func MiddlewareAdd(handler http.Handler) {
	//App.Server.Handler.Use(middleware.RequestID)
}
func NewHttpSever() *HttpServer {
	app := &HttpServer{
		//Handlers: cr,
		//Server: ,
		//Log: ,
		//Cfg:      cfg,
	}

	return app
	//return NewHttpServerWithCfg()
}

func Run() {
	App.Log = logger.Setup(viper.GetString("env"))
	App.Server = &http.Server{
		Addr:                         "0.0.0.0:" + strconv.Itoa(viper.GetInt("port")),
		Handler:                      router.Routes, // Pass our instance of gorilla/mux in.
		DisableGeneralOptionsHandler: false,
		TLSConfig:                    nil,
		ReadTimeout:                  time.Duration(viper.GetInt("server.ReadTimeout")) * time.Millisecond,
		ReadHeaderTimeout:            time.Duration(viper.GetInt("server.ReadHeaderTimeout")) * time.Millisecond,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout:   time.Duration(viper.GetInt("server.WriteTimeout")) * time.Millisecond,
		IdleTimeout:    time.Duration(viper.GetInt("server.IdleTimeout")) * time.Millisecond,
		MaxHeaderBytes: 0,
		TLSNextProto:   nil,
		ConnState:      nil,
		ErrorLog:       nil,
		BaseContext:    nil,
		ConnContext:    nil,
	} //Handler = router.Routes
	go func() {
		//http.ListenAndServe
		if err := App.Server.ListenAndServe(); err != nil {
			//log.Error(err.Error())
			os.Exit(1)
		}
	}()

	App.Log.Info(
		"starting service users",
		slog.String("ENV", viper.GetString("env")),
		slog.String("PORT:", viper.GetString("port")),
		slog.String("version", "0.1"),
	)

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(viper.GetInt("server.GracefulTimeout")))
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	App.Server.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	//log.Info("shutting down")
	os.Exit(0)
	//return nil
}
func RequestJSON(w http.ResponseWriter, r *http.Request, jsonVar any) error {
	if r.Header.Get("Content-Type") != "application/json" {
		RenderError(w, errors.New("Content Type is not application/json"), http.StatusUnsupportedMediaType)
		return errors.New("Content Type is not application/json")
	}
	//
	var unmarshalErr *json.UnmarshalTypeError
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(jsonVar)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			RenderError(w, errors.New("Bad Request: Wrong Type provided for field "+unmarshalErr.Field), http.StatusBadRequest)
			//return errors.New("Bad Request. Wrong Type provided for field " + unmarshalErr.Field)
		}
		RenderError(w, errors.New("Bad Request: "+err.Error()), http.StatusBadRequest)
		//return errors.New("Bad Request " + err.Error())
	}
	return nil
}

func RenderJSON(w http.ResponseWriter, v interface{}, code int) {
	js, err := json.Marshal(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		RenderError(w, err, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func RenderError(w http.ResponseWriter, err error, httpErrorCode int) {
	js, err := json.Marshal(ResponsError{Error: err.Error()})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpErrorCode)
	w.Write(js)

}
