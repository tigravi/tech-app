package router

import (
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
)

var Routes *chi.Mux

type Route struct {
	Method    string
	Pattern   string
	HandlerFn http.HandlerFunc
}

var (
	//migration []interface{}
	RouteList []Route
)

func init() {
	Routes = chi.NewRouter()
}

func AddMethod(method string, pattern string, handlerFn http.HandlerFunc) {
	RouteList = append(RouteList, Route{
		Method:    method,
		Pattern:   pattern,
		HandlerFn: handlerFn,
	})
	log.Println("Router:", method, pattern)
}
func AddDefaultMiddlewares(middlewares ...func(http.Handler) http.Handler) {
	Routes.Use(middlewares...)
}
func Mount(pattern string, handler http.Handler) {
	Routes.Mount(pattern, handler)
}

func NewRoute() http.Handler {
	r := chi.NewRouter()
	for _, val := range RouteList {
		r.Method(val.Method, val.Pattern, val.HandlerFn)
	}
	return r
}
