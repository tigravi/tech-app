package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

var db *gorm.DB //база данных

func InitDB() {

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=%s password=%s timezone=%s",
		viper.GetString("database.host"),
		viper.GetString("database.user"),
		viper.GetString("database.name"),
		viper.GetString("database.sslmode"),
		viper.GetString("database.password"), //Создать строку подключения
		viper.GetString("database.timezone")) //Создать строку подключения
	fmt.Println(dbUri)

	conn, err := gorm.Open(viper.GetString("database.driver"), dbUri)
	if err != nil {
		fmt.Print(err)
	}

	db = conn

	//db.Debug().AutoMigrate(&Account{}) //Миграция базы данных
}

// возвращает дескриптор объекта DB
func GetDB() *gorm.DB {
	return db
}
